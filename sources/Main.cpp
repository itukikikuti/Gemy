#include "XLibrary11.hpp"
#include "XModelLibrary11.hpp"

using namespace XLibrary11;

int Main() {
	Model gem(L"assets/Gem.fbx");
	Camera camera;
	camera.position = Float3(0.0f, 1.0f, -5.0f);
	gem.meshes[0]->material.Load(L"assets/Gem.hlsl");
	gem.meshes[0]->SetCullingMode(D3D11_CULL_FRONT);

	while (App::Refresh()) {
		camera.Update();

		gem.angles.y += App::GetDeltaTime() * 50.0f;
		gem.Draw();
	}

	return 0;
}
